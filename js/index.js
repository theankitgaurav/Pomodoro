var timer
var app = new Vue({
  el: ".app",
  data: {
    timerLength: 25,
    remaining: 0,
    status: "Start",
    minutesLeft: 0,
    secondsLeft: 0
  },
  methods: {
    togglePomodoro: function() {
      var self = this
      if (this.status === "Start") {
        self.status = "Reset"
        self.minutesLeft = self.timerLength - 1;
        self.secondsLeft = 60;
        timer = setInterval(function() {
          if (self.minutesLeft === 0 && self.secondsLeft === 0) {
            self.timerOver();
          } else if (self.secondsLeft === 0) {
            self.minutesLeft--;
            self.secondsLeft = 60;
          } else {
            self.secondsLeft--;
          }
        }, 1000);
      } else if (this.status === "Reset") {
        self.timerOver();
      }
    },
    timerOver: function() {
      this.status = "Start"
      this.timerLength = 25
      clearInterval(timer)
    }
  }
})